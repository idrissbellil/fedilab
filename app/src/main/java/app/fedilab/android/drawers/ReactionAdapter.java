package app.fedilab.android.drawers;
/* Copyright 2020 Thomas Schneider
 *
 * This file is a part of Fedilab
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * Fedilab is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Fedilab; if not,
 * see <http://www.gnu.org/licenses>. */


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import app.fedilab.android.R;
import app.fedilab.android.client.Entities.Reaction;
import app.fedilab.android.helper.Helper;


/**
 * Created by Thomas on 10/03/2020.
 * Adapter for reactions on messages
 */
public class ReactionAdapter extends RecyclerView.Adapter {

    private List<Reaction> reactions;

    ReactionAdapter(List<Reaction> reactions) {
        this.reactions = reactions;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int position) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        return new ViewHolder(layoutInflater.inflate(R.layout.drawer_reaction, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        final Reaction reaction = reactions.get(viewHolder.getAdapterPosition());
        ViewHolder holder = (ViewHolder) viewHolder;

        holder.reaction_count.setText(String.valueOf(reaction.getCount()));
        if(reaction.isMe()) {
            holder.reaction_container.setBackgroundResource(R.drawable.reaction_voted);
        }else{
            holder.reaction_container.setBackgroundResource(R.drawable.reaction_border);
        }
        if(reaction.getUrl() != null){
            holder.reaction_name.setVisibility(View.GONE);
            holder.reaction_emoji.setVisibility(View.VISIBLE);
            holder.reaction_emoji.setContentDescription(reaction.getName());
            Helper.loadGiF(holder.itemView.getContext(), reaction.getUrl(), holder.reaction_emoji);
        }else{
            holder.reaction_name.setText(reaction.getName());
            holder.reaction_name.setVisibility(View.VISIBLE);
            holder.reaction_emoji.setVisibility(View.GONE);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return reactions.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView reaction_name, reaction_count;
        LinearLayout reaction_container;
        ImageView reaction_emoji;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            reaction_name = itemView.findViewById(R.id.reaction_name);
            reaction_count = itemView.findViewById(R.id.reaction_count);
            reaction_container = itemView.findViewById(R.id.reaction_container);
            reaction_emoji = itemView.findViewById(R.id.reaction_emoji);
        }
    }


}